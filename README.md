# Sicredi


Projeto de Automação de Testes visando automatizar a API de Simulação de Crédito que contempla os Endpoints Restrições e Simulações

Para se executar esse projeto deve ter instalado na maquina e configurado nas variaveis de ambiente Java 1.8 ou Acima e Deve Possuir o Maven 3.8.1 ou acima.

Para subir o swagger deve rodar o comando **mvn clean spring-boot:run** no cmd tendo que ter na maquina o projeto Desafio Automação de Teste API

