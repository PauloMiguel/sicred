package Api;

import Sicred.Base_Api;


import static io.restassured.RestAssured.*;

import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.config.RestAssuredConfig;
import io.restassured.response.Response;
import junit.framework.Assert;

import static org.hamcrest.Matchers.*;

import java.util.Arrays;



public class Restricoes extends Base_Api {
	
	@Test
	
	public void BuscaPessoaComRestricao() {
		
		given().
		pathParam ("cpf", "97093236014").
		
		when().
		get("/v1/restricoes/{cpf}").
		
		then().
		
		statusCode(200).
		body("mensagem", is ("O CPF 97093236014 tem problema"));
		
		
	}
	

	@Test
	
	public void BuscaPessoaSemRestricao() {
		
		given().
		pathParam ("cpf", "13052639013").
		
		when().
		get("/v1/restricoes/{cpf}").
		
		then().
		
		statusCode(204);
		
	}
	
	
}
