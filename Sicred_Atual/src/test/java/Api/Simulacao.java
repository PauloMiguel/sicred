package Api;

import Sicred.Base_Api;


import static io.restassured.RestAssured.*;

import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.config.RestAssuredConfig;
import io.restassured.response.Response;
import junit.framework.Assert;

import static org.hamcrest.Matchers.*;

import java.util.Arrays;

public class Simulacao extends Base_Api {
	
	
@Test
	
	public void CadastrarSimulacao() {
		
		given()
		.contentType("application/json")
		.body("{\"nome\": \"Paulo SiCred_\",\"cpf\" : \"90516575014\", \"email\":\"meuemail@email.com\",\"valor\": \"1200\",\"parcelas\": \"3\", \"seguro\": \"true\"}").	
		
		when().
		post("/v1/simulacoes").
		
		then().
		
		statusCode(201);
	  
	}


@Test

public void ValidarSimulacaoSemCPF() {
	
	given()
	.contentType("application/json")
	.body("{\"nome\": \"Paulo SiCred_\",\"email\":\"meuemail@email.com\",\"valor\": \"1200\",\"parcelas\": \"3\", \"seguro\": \"true\"}").	
	
	when().
	post("/v1/simulacoes").
	
	then().
	
	statusCode(400);
  
}


@Test

public void ValidarSimulacaoSemNome() {
	
	given()
	.contentType("application/json")
	.body("{\"cpf\" : \"73296596062\", \"email\":\"meuemail@email.com\",\"valor\": \"1200\",\"parcelas\": \"3\", \"seguro\": \"true\"}").	
	
	when().
	post("/v1/simulacoes").
	
	then().
	
	statusCode(400);
  
}


@Test

public void ValidarSimulacaoSemEmail() {
	
	given()
	.contentType("application/json")
	.body("{\"nome\": \"Paulo SiCred_\",\"cpf\" : \"73296596062\",\"valor\": \"1200\",\"parcelas\": \"3\", \"seguro\": \"true\"}").	
	
	when().
	post("/v1/simulacoes").
	
	then().
	
	statusCode(400);
  
}



@Test

public void ValidarSimulacaoSemValor() {
	
	given()
	.contentType("application/json")
	.body("{\"nome\": \"Paulo SiCred_\",\"cpf\" : \"73296596062\", \"email\":\"meuemail@email.com\",\"parcelas\": \"3\", \"seguro\": \"true\"}").	
	
	when().
	post("/v1/simulacoes").
	
	then().
	
	statusCode(400);
  
}


@Test

public void ValidarSimulacaoSemParcela() {
	
	given()
	.contentType("application/json")
	.body("{\"nome\": \"Paulo SiCred_\",\"cpf\" : \"73296596062\", \"email\":\"meuemail@email.com\",\"valor\": \"1200\",\"seguro\": \"true\"}").	
	
	when().
	post("/v1/simulacoes").
	
	then().
	
	statusCode(400);
  
}

@Test

public void ValidarSimulacaoSemSeguro() {
	
	given()
	.contentType("application/json")
	.body("{\"nome\": \"Paulo SiCred_\",\"cpf\" : \"73296596062\", \"email\":\"meuemail@email.com\",\"valor\": \"1200\",\"parcelas\": \"3\"}").
	
	when().
	post("/v1/simulacoes").
	
	then().
	
	statusCode(500);
	
	
	//nesse endpoint est apresentando erro 500 conforme abaixo:
	
	/*{
	    "timestamp": "2021-06-14T02:06:36.717+0000",
	    "status": 500,
	    "error": "Internal Server Error",
	    "message": "ModelMapper mapping errors:\r\n\r\n1) Error mapping br.com.sicredi.simulacao.dto.SimulacaoDTO to br.com.sicredi.simulacao.entity.Simulacao\r\n\r\n1 error",
	    "path": "/api/v1/simulacoes"
	}
	
	*/
  
}



@Test

public void CadastroCpfDuplicado() {
	
	given()
	.contentType("application/json")
	.body("{\"nome\": \"Paulo SiCred_\",\"cpf\" : \"13052047001\", \"email\":\"meuemail@email.com\",\"valor\": \"1200\",\"parcelas\": \"3\", \"seguro\": \"true\"}").	
	
	when().
	post("/v1/simulacoes").
	
	then().
	
	statusCode(400).
	body("mensagem", is ("CPF duplicado"));
  
}


@Test

public void AlterarSimulaoNome() {
	
	given()
	.contentType("application/json")
	.body("{\"nome\": \"Paulo SiCred_Automacao\",\"cpf\" : \"23154303057\", \"email\":\"meuemail@email.com\",\"valor\": \"1200\",\"parcelas\": \"3\", \"seguro\": \"true\"}").	
	
	
	when().
	put("/v1/simulacoes/23154303057").
	
	then().
	
	statusCode(200);
  
}

@Test

public void AlterarSimulaoEmail() {
	
	given()
	.contentType("application/json")
	.body("{\"nome\": \"Paulo SiCred_Automacao\",\"cpf\" : \"23154303057\", \"email\":\"meuemail@teste.com\",\"valor\": \"1200\",\"parcelas\": \"3\", \"seguro\": \"true\"}").	
	
	
	when().
	put("/v1/simulacoes/23154303057").
	
	then().
	
	statusCode(200);
  
}


@Test

public void AlterarSimulaoValor() {
	
	given()
	.contentType("application/json")
	.body("{\"nome\": \"Paulo SiCred_Automacao\",\"cpf\" : \"23154303057\", \"email\":\"meuemail@teste.com\",\"valor\": \"1560000\",\"parcelas\": \"3\", \"seguro\": \"true\"}").	
	
	
	when().
	put("/v1/simulacoes/23154303057").
	
	then().
	
	statusCode(200);
  
}



@Test

public void AlterarSimulaoParcela() {
	
	given()
	.contentType("application/json")
	.body("{\"nome\": \"Paulo SiCred_Automacao\",\"cpf\" : \"23154303057\", \"email\":\"meuemail@teste.com\",\"valor\": \"1560000\",\"parcelas\": \"3\", \"seguro\": \"true\"}").	
	
	
	when().
	put("/v1/simulacoes/23154303057").
	
	then().
	
	statusCode(200);
  
}


@Test

public void AlterarSimulaoSeguro() {
	
	given()
	.contentType("application/json")
	.body("{\"nome\": \"Paulo SiCred_Automacao\",\"cpf\" : \"23154303057\", \"email\":\"meuemail@teste.com\",\"valor\": \"1560000\",\"parcelas\": \"5\", \"seguro\": \"false\"}").	
	
	
	when().
	put("/v1/simulacoes/23154303057").
	
	then().
	
	statusCode(200);
  
}

@Test

public void ConsultaTodasSimulaesCadastradas() {
	
	given()
    .contentType("application/json").
	
	when().
	get("/v1/simulacoes").
	
	then().
	
	statusCode(200);
	
	
	
}


@Test

public void ConsultaSimulaoPorCPF() {
	
	given()
    .contentType("application/json").
	
	when().
	get("/v1/simulacoes/23154303057").
	
	then().
	
	statusCode(200);
	
	
	
}


@Test

public void ConsultaSimulaoPorCPFNoCadastrado() {
	
	given()
    .contentType("application/json").
	
	when().
	get("/v1/simulacoes/91321205082").
	
	then().
	
	statusCode(404);
	
	
	
}


@Test

public void RemoverSimulao() {
	
	given()
    .contentType("application/json").
	
	when().
	delete("/v1/simulacoes/60").
	
	then().
	
	statusCode(200);

	
}






}
